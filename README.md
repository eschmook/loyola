# Loyola University New Orleans :: Drupal 8 Theme
### This theme uses the [Bootstrap 3 framework](https://getbootstrap.com/docs/3.3/).

*NOTE: At time of development, this theme was based upon [Bootstrap 3.3.7](https://getbootstrap.com/docs/3.3/), as Bootstrap 4 had not yet been released.*

Theme components use **[flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)** for layout, but the normal [Bootstrap layout grid](https://getbootstrap.com/docs/3.3/css/#grid) can also be applied throughout the site.

Documentation specifically for the [theme's standard grid](docs/grid.md) can be found at `./docs/grid.md`.

## Overview
------
- You must be familiar with the basic concept of using the [Sass] CSS pre-processor.
- Editing styles should be done with files ending in the `.scss` extension, and **not** in files ending in `.css`.
- You must use a [Sass] compiler to compile the various Sass modules.
- Theme-specific JavaScript modules are initialized via the `router.js` file.

## Setup
------
A gulp task workflow is included with this theme to automate compiling both CSS and JS along with other helper tasks for development. This file (`gulpfile.js`) can be run from the `/assets/` directory by typing `gulp` (development) or `gulp build` (production) from  your **[command line](https://gulpjs.org/CLI.html)**.

  - When using the included `gulpfile.js` workflow, run `npm install` from your command line to install the necessary dependencies referenced in the `package.json` file (see https://www.npmjs.com/ for installation).
  - [GUI Sass compilers](http://koala-app.com) are also available for various platforms, but normally these compilers __*will not compile any JavaScript changes*__.

## Structure
------
- The `/assets/` directory contains the source files for compiling the custom CSS and JS source files, as well as the `/bootstrap` subfolder containing bootstrap's fonts, CSS and JS files.
- `/css/` and `/js/` hold the compiled custom CSS and JS output.
  - `/css/style.css` is the primary stylesheet for the site as viewed by the public.
  - `/css/admin.css` houses styles visible from within the site admin.
- `/images/` houses any decorative images used in the theme. _This folder should **not** be used for content imagery normally handled by the CMS._
- `/templates/` is the location for all of the custom [Twig](https://twig.symfony.com/doc/2.x/) template files used on blocks, components, views, etc. in the Content Management System (CMS).

## CSS/Sass
-----
- The `./assets/sass/base/_variables.scss` stylesheet assigns default variables that should be used by the [Bootstrap Framework].

- The `./assets/sass/style.scss` master stylesheet imports all of the various `.scss` files for output. This file will then be compiled to `./css/loyola.css` for display on the site.
  - Bootstrap base styles are imported from within this file.
  - Generally speaking, **you will not need to modify this file** unless you need to add or remove files to be imported.
  - Follow the order as specified within the file when adding new components to ensure Sass variables are properly defined when called.

- The `./assets/sass/admin.scss` stylesheet imports severl of the base `.scss` files (fonts, links, buttons, tables, etc.) so that Drupal content editors can preview text styles while creating and editing content in WYSIWYG areas. This file is compiled to `./css/loyola-admin.css`.
  - As with the `style.scss` master stylesheet, _you should not need to modify this file._ Changes to text and links at basic levels should be performed in their respective modules (for example, `./css/base/_typography.scss`).

- Global, component and template-level modules are separated into subfolders in the `./assets/sass/` directory:

  - `/base/` refers to the most rudimentary level of theming (typography, tables, images, form elements).
  - `/lib/` adds any _base-level_ third-party library, module, and font styling. (**Note:** Style overrides for these items are contained within `_overrides.scss`).
  - `/global/` contains styling for modules throughout the site such as the main navigation, footer, etc
  - `/components/` includes custom components. Similar components are grouped into subfolders.
  - `/templates/` contains view- and template-level styling and overrides.

## JavaScript
-----
- A variant of the *[revealing module pattern](https://addyosmani.com/resources/essentialjsdesignpatterns/book/#revealingmodulepatternjavascript)* is used for this theme's JavaScript functions. This helps prevent littering the global (`window`) namespace with our theme's various functions, and avoids potential conflicts with scripts from other sources.
- Base, component and third-party JS modules are separated into subfolders in the `./assets/js/` directory:

  - `/base/` initializes and routes the Loyola theme JS functions (namespaced under `LOY`).
  - `/lib/` adds third-party JS libraries from modules or external sources.
  - `/modules/` defines functions for the individual `LOY` namespaced JS modules (ex. `LOY.util` or `LOY.hero`).

## Theme libraries and regions
-----
- `./loyola.info.yml` defines the available regions for content display in the CMS.
  - These regions are visible in the Drupal admin by navigating to `Structure > Block layout` in the `Manage` menu.
  - The `./loyola.info.yml` file also initializes the internal and external CSS & JS libraries for the theme, as defined in `loyola.libraries.yml`.

-----
### See also:
- @link theme_settings Theme Settings @endlink
- @link templates Templates @endlink
- @link plugins Plugin System @endlink

[Bootstrap 3 Framework]: http://getbootstrap.com/docs/3.3/
[Bootstrap Framework Source Files]: https://github.com/twbs/bootstrap-sass
[Sass]: http://sass-lang.com
