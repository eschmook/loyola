var themeName = 'loyola',
    themeDir = 'sass/',
    mainStylesheet = themeDir + 'style.scss',
    adminStylesheet = themeDir + 'admin.scss';

var gulp = require("gulp"),
    sass = require("gulp-sass"),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require("gulp-autoprefixer"),
    browserSync = require('browser-sync').create(),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    shell = require('gulp-shell'),
    pump = require("pump"),
    js_files = [
        'js/lib/*.js',
        'js/base/define.js',
        'js/modules/*.js',
        'js/base/router.js'
    ];


// Compile SCSS files to CSS
gulp.task("scss", function() {
    gulp.src(mainStylesheet)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle : "compact"
        }))
        .pipe(autoprefixer({
            browsers : ["last 20 versions"]
        }))
        .pipe(rename('loyola.css'))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest("../css/"))
        .pipe(browserSync.stream());
});

// Compile Admin SCSS files to CSS
gulp.task("admin_styles", function() {
    gulp.src(adminStylesheet)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle : "compressed"
        }))
        .pipe(autoprefixer({
            browsers : ["last 20 versions"]
        }))
        .pipe(rename('loyola-admin.css'))
        // .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest("../css/"))
        .pipe(browserSync.stream());
});

// Compile SCSS files to CSS
gulp.task("compress_css", function() {
    gulp.src(mainStylesheet)
        .pipe(sass({
            outputStyle : "compressed"
        }))
        .pipe(autoprefixer({
            browsers : ["last 20 versions"]
        }))
        .pipe(rename('loyola.css'))
        .pipe(gulp.dest("../css/"))
        .pipe(browserSync.stream());
});

// Compile JS
gulp.task("js", function() {
    gulp.src(js_files)
        .pipe(sourcemaps.init())
        .pipe(concat('loyola.js'))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest("../js/"))
        .pipe(browserSync.stream());
});

// Compile and compress JS
gulp.task("compress_scripts", function(callback) {
    pump([
        gulp.src(js_files),
        concat('loyola.js'),
        uglify(),
        gulp.dest("../js/")
    ],
    callback
    )
    .pipe(browserSync.stream());
});

// Reload Drupal cache for template updates
gulp.task('twig', ['refreshCache'], function() {
    console.info('Twig file updated, refreshing cache.');
    browserSync.reload();
});

gulp.task('refreshCache', shell.task(['lando drush cc render']));

// Watch asset folder for development changes
gulp.task("watch", ["scss", "admin_styles", "js"], function() {
    browserSync.init({
        proxy: 'http://' + themeName + '.lndo.site',
        open: false,
        notify: true,
        reloadOnRestart: true,
        scrollThrottle: 150
    });
    gulp.watch("sass/**/*", ["compress_css"]);
    gulp.task(adminStylesheet, ["admin_styles"]);
    gulp.watch("js/**/*", ["js"]);
    gulp.watch('../templates/**/*.twig', ['twig']);
});

// Build files for production
gulp.task("build", ["compress_css", "admin_styles", "compress_scripts"], function() {
    gulp.task(mainStylesheet, ["compress_css"]);
    gulp.task(adminStylesheet, ["admin_styles"]);
    gulp.task("js/**/*", ["compress_scripts"]);
});

// Set watch as default task
gulp.task("default", ["watch"]);

