LOY.router = (function(LOY, $) {


	function init() {
		// ** always run **
		LOY.util.init();

		// ** conditional modules **
		if (document.querySelector('.header__main') !== null) {
			// site navigation
			LOY.header.init();
		}

		if (document.querySelector('.sidebar') !== null || document.querySelector('.has-gutter') !== null) {
			// sidebar functions
			LOY.sidebar.init();
		}

		if (document.querySelector('.lazyload') !== null || document.querySelector('.lazyload--bkgd') !== null) {
			// image lazyloader
			LOY.images.init();
		}

		if (document.querySelector('.video__inline') !== null) {
			// video player
			LOY.video.init();
		}

		if (document.querySelector('.js-form-required') !== null) {
			// webform validation
			LOY.webforms.init();
		}

		if (document.querySelector('.quicklinks') !== null) {
			// quick links
			LOY.quicklinks.init();
		} else {
			// back to top enabled (quick links disabled)
			LOY.affixItem.topLink();
		}

		if (document.querySelector('.hero') !== null) {
			// hero component
			LOY.hero.init();
		}

		if (document.querySelector('.field--item') !== null) {
			// drupal paragraphs template
			LOY.paragraphs.init();
		}

		if (document.querySelector('.carousel') !== null) {
			// carousel component
			LOY.carousel.init();
		}

		if (document.querySelector('.tabs') !== null) {
			// tabbed component
			LOY.tabs.init();
		}

		if (document.querySelector('.finder-view') !== null) {
			// program finder-specific functions
			LOY.programfinder.init();
		} else if (document.querySelector('.content .views-element-container') !== null || document.querySelector('.card-view') !== null) {
			// base view functions
			LOY.views.init();
		}

		if (document.querySelector('.masonry-view') !== null) {
			// base view functions
			LOY.views.init();
			// masonry grid
			LOY.masonry.init();
		}

		if (document.querySelector('.page-node-type-person') !== null) {
			// sticky sidebar
			LOY.affixItem.stickyItem('.profile__sidebar');
		}
	}


	return {
		init: init
	};

})(LOY, jQuery);

LOY.router.init();