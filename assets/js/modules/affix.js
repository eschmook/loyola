LOY.affixItem = (function(LOY, $) {
	// uses stickybits polyfill for browsers that do not fully support position: sticky in CSS

	// sticky 'back to top' button
	function topLink() {
		var $topBtn = $('.back-to-top'),
			stickyHeight = -600;

		stickybits($topBtn, {
			useStickyClasses: true,
			// stickyBitStickyOffset: stickyHeight,
			noStyles: true
		});
	}


	// generic 'sticky' item
	function stickyItem(ele) {
		var $item = $(ele);

		stickybits($item, {
			useStickyClasses: true,
			noStyles: true
		});
	}



	return {
		topLink: topLink,
		stickyItem: stickyItem
	};

})(LOY, jQuery);