LOY.carousel = (function(LOY, $) {
	var $carousel = $('.carousel'),
		desktopSize = LOY.desktopSize || 992,
		flick;



	function init() {
		buildCarousels();
	}


	// determine individual carousel options
	// overrides default behavior at template level (see https://flickity.metafizzy.co/options.html)
	function carouselOptions(ele) {
		return {
			wrap: (ele.dataset.sliderWrap === 'true' ? true : false),
			adapt: (ele.dataset.sliderAdaptiveheight === 'true' ? true : false),
			lazy: (ele.dataset.sliderLazy === 'true' ? true : false),
			fade: (ele.dataset.sliderFade === 'false' ? false : true),
			dots: (ele.dataset.sliderDots === 'false' ? false : true),
			buttons: (ele.dataset.sliderButtons === 'true' ? true : false),
			height: (ele.dataset.sliderSetheight === 'false' ? false : true),
		};
	}


	// construct flickity object
	function carouselItem(slider, options) {
		return new Flickity(slider, {
			watchCSS: true,
			adaptiveHeight: options.adapt,
			setGallerySize: options.height,
			lazyLoad: options.lazy,
			imagesLoaded: true,
			pageDots: options.dots,
			prevNextButtons: options.buttons,
			wrapAround: options.wrap,
			on: {
				ready: function() {
					if (options.fade) {
						fadeCarousel(slider);
					}
				}
			}
		});
	}


	// initiate each carousel
	function buildCarousels(ele) {
		var $ele = ele || $carousel.not($('.flickity-enabled')),
			flick;

		$ele.each(function(i, slider) {
			var settings = carouselOptions(slider);
				flick = carouselItem(slider, settings);

			if (!settings.dots) {  // enable arrows when nav dots are disabled
				navigateCarousel($carousel, flick);
				updateSlideStatus(flick);

				flick.on('select', function() {
					updateSlideStatus(flick);
					LOY.images.lazyImage('.lazyload');
				});
			} else {
				flick.on('select', function() {
					setTimeout(function() {
						LOY.images.lazyImage('.carousel .is-selected .lazyload');
					}, 350);
				});
			}
		});
	}


	// update slide counter text on selection
	function updateSlideStatus(carousel) {
		var $counter = $('.slider__count'),
			currSlide = $counter.find('.slide--current'),
			totalSlide = $counter.find('.slide--total'),
			slideNum = carousel.selectedIndex + 1;

		currSlide.text(slideNum);
		totalSlide.text(carousel.slides.length);
	}


	// fade gallery into view
	function fadeCarousel(ele) {
		if (ele.classList.contains('carousel-active')) {
			ele.classList.remove('carousel-active');
		} else {
			ele.classList.add('carousel-active');
		}
	}


	// bind events to carousel buttons
	function navigateCarousel(carousel, instance) {
		var $sliderWrap = carousel.parents('.slider__wrapper'),
			$prevButton = $sliderWrap.find('.prev'),
			$nextButton = $sliderWrap.find('.next');

		$prevButton.on('click', function() {
			instance.previous();
		});
		$nextButton.on('click', function() {
			instance.next();
		});
	}




	return {
		init: init
	};

})(LOY, jQuery);