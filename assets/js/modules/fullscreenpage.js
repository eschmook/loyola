LOY.fullscreenpage = (function(LOY, $) {
	var fullPageContent,
		desktopSize = LOY.desktopSize || 992;



	function init() {
		// initScroller('scroll-lib');

		bindEvents();
	}



	function initScroller(scriptName) {
		var available = (document.getElementById(scriptName) ? true : false);
		var interval = setInterval(function() {
			console.log('checking for script: ' + available);
			if (window.page !== undefined) {
				console.info('pager loaded');
				clearInterval(interval);
				pageContent();
			}
		}, 800);
	}



	function pageContent() {
		console.info('loading scroller');

	}



	function bindEvents() {  // event handlers
		$(window).on('resize', LOY.util.debounce(function() {

		}, 400));
	}




	return {
		init: init
	};

})(LOY, jQuery);