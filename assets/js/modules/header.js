LOY.header = (function(LOY, $) {
	var $body = $('body'),
		desktopSize = LOY.desktopSize || 992,
		$header = $('.header__main'),
		$navButton = $('#mobilenav-button'),
		$mobileDrop = $header.find('.dropdown');



	function init() {
		checkScreen();
		bindEvents();
	}


	// determine screen size for mobile or desktop menu
	function checkScreen() {
		if ($(window).width() > desktopSize) {
			// force mobile menu off on desktop
			mobileMenuToggle('off');
			LOY.util.resetAllClasses('.navlevel--0', 'open');
		}
	}


	// control state of mobile menu
	function mobileMenuToggle(forceState) {
		if ($body.hasClass('mobilenav--open') || forceState === 'off' ) {
			// close menu if open
			$body.removeClass('mobilenav--open');
			LOY.util.lockScroll('off');
		} else {
			// open menu and lock page scrolling
			$body.addClass('mobilenav--open');
			LOY.util.lockScroll();
		}
	}


	// for browsers that don't support focus-within for tabbing
	function focusNav(ele) {
		if (ele.parents('.dropdown--main').length) {
			LOY.util.primaryFocusNav(ele, '.dropdown--main');  // primary nav dropdown

			if (ele.parents('.navlevel--1').length) {  // secondary nav dropdown
				LOY.util.secondaryFocusNav(ele, '.navlevel--1');
			}

		} else if (ele.parents('.dropdown').length) {  // standard dropdown menus
			LOY.util.primaryFocusNav(ele, '.dropdown');

		} else {  // no dropdown
			$('.dropdown, .dropdown--main').removeClass('focus-nav');
		}
	}




	function bindEvents() {  // event handlers
		// throttle screen size check to prevent performance hit
		$(window).on('resize', LOY.util.debounce(function() {
			checkScreen();
		}, 400));

		// toggle mobile nav on button trigger
		$navButton.on('click', function(e) {
			e.stopImmediatePropagation();
			mobileMenuToggle();
		});

		// toggle mobile submenu display
		$mobileDrop.on('click', function(e) {
			if ($(window).width() < desktopSize) {
				e.stopImmediatePropagation();
				e.preventDefault;
				$(this).toggleClass('open');
			}
		});

		// toggle mobile nav on underlay click
		$body.on('click', '.loyola__content, .header__underlay', function() {
			if ($body.hasClass('mobilenav--open')) {
				mobileMenuToggle('off');
			}
		});

	}





	return {
		init: init,
		focusNav: focusNav
	};

})(LOY, jQuery);