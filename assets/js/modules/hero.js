LOY.hero = (function(LOY, $) {
	var $hero = $('.hero'),
		$videoPlayBtn = $hero.find('.hero__videobutton'),
		$videoCloseBtn = $hero.find('.hero__videoclose');



	function init() {
		bindEvents();
	}



	// play or pause embedded HTML5 video
	// NOTE: third party API JS located at twig template level
	function toggleLocalVideo(parent, videoAction, evt) {
		var $inlineVideo = $(parent).find('video')[0];

		if (videoAction === 'play') {
			$inlineVideo.volume = 1;
			$inlineVideo.play();
		} else {
			$inlineVideo.volume = 0;
			$inlineVideo.pause();
		}
	}



	function bindEvents() {  // event handlers
		// show video on play button click
		$videoPlayBtn.on('click', function(e) {
			var $el = $(this),
				$heroContent = $el.parents('.hero__content'),
				$heroVid = $heroContent.siblings('.hero__video');

			$heroVid.toggleClass('active');
			$heroContent.siblings('.hero__image').toggleClass('hidden');
			$heroContent.toggleClass('hidden');

			if ($heroVid.hasClass('video--local')) {
				toggleLocalVideo($heroVid, 'play', e);
			}
		});

		// hide video on close button click
		$videoCloseBtn.on('click', function(e) {
			var $el = $(this),
				$heroVid = $el.parents('.hero__video');

			$heroVid.siblings('.hero__content, .hero__image').toggleClass('hidden');
			$heroVid.toggleClass('active');

			if ($heroVid.hasClass('video--local')) {
				toggleLocalVideo($heroVid, 'pause', e);
			}
		});
	}



	return {
		init: init
	};

})(LOY, jQuery);
