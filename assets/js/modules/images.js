LOY.images = (function(LOY, $) {
	var lazyLoader = LazyLoad || {};



	function lazyImage(element) {
		var images = Array.prototype.slice.apply(document.querySelectorAll(element));

		images.forEach(function(img) {
			if (!img.classList.contains('loaded')) {
				return new lazyLoader({
					elements_selector: element,
					threshold: 100,
				});
			}
		});
	}


	function init() {
		if (document.querySelector('.lazyload') !== null) {
			lazyImage('.lazyload');
		}

		if (document.querySelector('.lazyload--bkgd') !== null) {
			lazyImage('.lazyload--bkgd');
		}
	}


	return {
		init: init,
		lazyImage: lazyImage
	};

})(LOY, jQuery);