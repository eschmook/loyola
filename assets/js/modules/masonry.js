LOY.masonry = (function(LOY, $) {
	var desktopSize = LOY.desktopSize || 992,
		masonryOptions = {},
		theGrid;



	function init() {
		// initialize masonry view on page load
		if (document.querySelector('.masonry-view') !== null) {
			masonryOptions = gridOptions('.masonry-view');
			buildMasonryGrid('.masonry-view', masonryOptions);
		}

		bindEvents();
	}



	// construct options object for masonry initialization
	function gridOptions(ele) {
		var $el = document.querySelector(ele);

		return {
			item: $el.dataset.gridItem || '.card',
			gutter: parseFloat($el.dataset.gridGutter) || 15,
		};
	}


	// masonry construction
	function buildMasonryGrid(container, options) {
		theGrid = $(container).masonry({
			itemSelector: options.item,
			columnWidth: options.item,
			gutter: options.gutter || 30,
			stagger: 30,
			percentPosition: true,
		});

		if ($(window).width() > desktopSize) {
			theGrid.imagesLoaded().progress(function() {
				theGrid.masonry('layout');
				theGrid.on('layoutComplete', fadeGrid('in', container));
				// console.info('masonry build');
			});
		} else {
			theGrid.masonry('destroy');
			theGrid.on('layoutComplete', fadeGrid('out', container));
			// console.info('masonry destroyed');
		}
	}


	// animate in/out grid
	function fadeGrid(evt, ele) {
		var grid = document.querySelector(ele);

		if (evt === 'out') {
			grid.classList.remove('visible');
		} else {
			grid.classList.add('visible');
		}
	}



	function bindEvents() {  // event handlers
		// build or destroy masonry depending on screen size
		$(window).on('resize', LOY.util.debounce(function() {
			if (document.querySelector('.masonry-view') !== null) {
				buildMasonryGrid('.masonry-view', masonryOptions);
			}
		}, 500));

		// rebuild masonry on ajax load
		$(document).ajaxComplete(function() {
			if (document.querySelector('.masonry-view') !== null) {
				// console.info('ajax complete');
				theGrid.masonry('reloadItems');
			}
		});

	}




	return {
		init: init,
		gridOptions: gridOptions,
		buildMasonryGrid: buildMasonryGrid
	};

})(LOY, jQuery);