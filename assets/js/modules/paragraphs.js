LOY.paragraphs = (function(LOY, $) {


	function init() {
		wrapTables();
		wrapWYSIWYG();
		setGalleryToutHeights();
		bindEvents();
	}



	// wrap mobile table structure on overflow and fade content
	function wrapTables() {
		if (document.querySelector('table') !== null) {
			LOY.util.wrapItems('.field--item > table', 'table-responsive');
			LOY.util.wrapItems('.table-responsive', 'table--wrap');
		}
	}


	// wrap wysiwyg columns for consistent spacing
	function wrapWYSIWYG() {
		if (document.querySelector('.ckeditor-col-container') !== null) {
			$('.ckeditor-col-container').wrapInner('<div class="ckeditor-row" />');
		}
	}


	// set image gallery tout heights
	function setGalleryToutHeights() {
		if (document.querySelector('.gallery-tout') !== null) {
			LOY.tabs.matchTabHeights('.gallery-tout__wrapper > .tout-item', '.gallery-tout__wrapper > .tout-item', 'reset');
			LOY.tabs.matchTabHeights('.gallery-tout__wrapper > .tout-item', '.gallery-tout__wrapper > .tout-item');
		}
	}



	function bindEvents() {  // event handlers
		// toggle aria attributes on active tab elements
		$('.tab-nav__anchor').on('show.bs.tab', function(e) {
			var $tabID = e.target.getAttribute('href').replace('#',''),
				$allTabs = $('.tab-pane'),
				$tabTarget = document.getElementById($tabID);

			$allTabs.each(function() {
				$(this).attr('aria-hidden', true);
			});
			$tabTarget.setAttribute('aria-hidden', false);
		});

		$('.gallery-tout').on('click focusin', '.tout-item', function(e) {
			$('.tout-item.active').removeClass('active');
			$(this).toggleClass('active');
		});

		$(window).on('resize', LOY.util.debounce(function() {
			setGalleryToutHeights();
		}, 500));
	}



	return {
		init: init
	};

})(LOY, jQuery);
