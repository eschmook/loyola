LOY.programfinder = (function(LOY, $) {
    var $finder = document.querySelector('.finder-view'),
        $fieldset = document.querySelector('.fieldgroup'),
        $underlay = document.querySelector('.finder__underlay'),
        currentFilterPath = getCurrentFilters(),
        resetTimer = 200;



    function init() {
        if (LOY.util.storageAvailable) {
            sessionStorage.getItem('currentURL') ? sessionStorage.removeItem('currentURL') : null;
            sessionStorage.setItem('currentURL', JSON.stringify(currentFilterPath));

            // console.info(sessionStorage.getItem('currentURL'));
        }

        LOY.affixItem.stickyItem('.finder-view__intro');

        loadingComplete();
        updateFilterCount();
        bindEvents();
    }


    // update the number of applied filters in filter menus
    function updateFilterCount(parent, flagAll) {
        var parentEle = parent || $('.form-elements'),
            $fieldsets = parentEle.find('fieldset');

        $fieldsets.each(function() {
            var $el = $(this),
                $counter = $el.find('.counter'),
                $allCheckboxes = $el.find('input[type="checkbox"]').length,
                checkedCount = ($el.find('input[type="checkbox"]:checked').length !== 0 ? $el.find('input[type="checkbox"]:checked').length : null);

            if ($allCheckboxes === checkedCount || flagAll === true) {  // set text to 'all' if all items checked
                $counter.text('All');
            } else {
                $counter.text(checkedCount);
            }
        });
    }


    // get values for current URL path
    function getCurrentFilters() {
        return {
            url: window.location.origin,
            filters: (window.location.hash.length ? window.location.hash.replace('#', '').split('&') : '')
        };
    }


    // toggle view update via forced click
    function updateView(ele) {
        var $btn = ele || $('body').find('.form-actions button[type=submit]');

        $finder.classList.add('loading');
        $btn.click();
    }


    // close filter list on user action
    function shrinkFilterList(shrinkTime) {
        var timer = shrinkTime || 200;

        setTimeout(function() {
            toggleUnderlay('off');
            $fieldset.classList.add('loading');
        }, timer);
    }


    // enable or disable underlay
    function toggleUnderlay(status) {
        if (status === 'on') {
            $underlay.hidden = false;
        } else {
            $underlay.hidden = true;
        }
    }


    // emulates focus-within behavior on non-supported browsers
    function focusFormFields() {
        var $focused = document.activeElement,
            $fieldset = $($focused).parents('.fieldset-wrapper'),
            $fieldParent = $focused.closest('.fieldgroup');

        // console.info('focused', $focused);
        // console.info('parent', $fieldset);
        if ($fieldset.length) {
            $fieldParent.classList.toggle('expanded');
        }
    }


    // disable loader
    function loadingComplete() {
        setTimeout(function() {
            updateFilterCount();
            $finder.classList.remove('loading');
            $fieldset.classList.remove('loading');
        }, resetTimer);
    }




	function bindEvents() {  // event handlers
        // remove default action on all button elements
        $('body').on('click', '.finder-view button', function(e) {
            e.preventDefault();
        });


        // toggle view of filter list
        $('body').on('click', '.finder-view__filters legend', function(e) {
            var $parentGroup = $(this).parents('.form-item');

            toggleUnderlay('off');
            if ($parentGroup.hasClass('expanded')) {
                $parentGroup.removeClass('expanded');
            } else {
                $('.finder-view__filters fieldset').removeClass('expanded');
                $parentGroup.addClass('expanded');
                toggleUnderlay('on');
            }
        });


        // keypress items
        $(document).on('keyup', function(e) {
            var keyNum = 'which' in e ? e.which : e.keyCode,  // cross-browser keycode values
                $parentGroup = $('.finder-view').find('fieldset');

            if (keyNum === 27 && $parentGroup.hasClass('expanded')) {  // esc key resets view
                $parentGroup.removeClass('expanded');
                // toggleUnderlay();
            } else if (keyNum === 9 && $('.no-focuswithin').length) {  // browsers that don't support focus-within for tabbing
                focusFormFields();
            }
        });


        // update individual filter count
        $('body').on('change', '.form-checkboxes input[type="checkbox"]', function(e) {
            var $parentGroup = $(this).parents('fieldset');

            e.preventDefault();
            e.stopPropagation();
            updateFilterCount();
        });


        // update tabs
        $('body').on('change', '.form-radios input[type="radio"]', function(e) {
            var $filterButton = $('body').find('.form-actions button[type=submit]');
            e.preventDefault();
            toggleUnderlay('off');
            updateView($filterButton);
        });


        // remove/reset all filters
        $('body').on('click', '.btn--reset', function(e) {
            var $parentGroup, $relatedCheckboxes,
                $allTabs = $('body').find('.form-radios .form-radio'),
                $filterButton = $('body').find('.form-actions button[type=submit]');

            $finder.classList.add('loading');
            shrinkFilterList();
            $allTabs.first().prop('checked', true);

            if ($(this).parents('fieldset').length) {  // filter group reset button
                $parentGroup = $(this).parents('fieldset');
                $relatedCheckboxes = $parentGroup.find('input[type="checkbox"]:checked');
            } else {  // empty results reset button
                $relatedCheckboxes = $('body').find('.form-checkboxes input[type="checkbox"]:checked');
            }

            $relatedCheckboxes.each(function() {
                $(this).prop('checked', false);
            });

            updateView($filterButton);
        });

        // apply filters
        $('body').on('click', '.btn--apply', function(e) {
            var $filterButton = $('.finder__wrapper').find('.form-actions button[type=submit]');

            $finder.classList.add('loading');
            shrinkFilterList();
            updateView($filterButton);
        });

        // drupal 8 doesn't have a callback for view ajaxing, so we have to sniff around for completion
        $(document).ajaxStart(function() {
			console.info('ajax init');
        });
        $(document).ajaxComplete(function() {
            loadingComplete();
        });
    }



	return {
		init: init
	};

})(LOY, jQuery);