LOY.quicklinks = (function(LOY, $) {
	var $body = $('body'),
		$quicklinkBtn = $('.quicklinks__button');



	function init() {
		// display quicklinks
		$body.addClass('quicklinks--enabled');

		bindEvents();
	}


	function bindEvents() {  // event handlers
		$quicklinkBtn.on('click', function(e) {
			var currentFocusItem = document.activeElement;

			LOY.util.lockScroll();
			$body.toggleClass('quicklinks--open');

			if (('.quicklinks--open').length) {
				// console.info('quicklinks open');
				document.getElementById('quicklinks__nav').focus();
			} else {
				currentFocusItem.focus();
			}
		});
	}





	return {
		init: init
	};

})(LOY, jQuery);