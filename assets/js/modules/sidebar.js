LOY.sidebar = (function(LOY, $) {
	var desktopSize = LOY.desktopSize || 992,
		$sidebar = $('.sidebar');



	function init() {
		bindEvents();
	}




	// toggle mobile sidebar menu
	function mobileSidebarMenu(ele, e) {
		if ($(window).width() < desktopSize) {
			e.preventDefault();
			ele.parent('nav').toggleClass('menu-active');
		}
	}


	// toggle desktop submenus open/closed
	function desktopSidebarMenu(ele, e) {
		var $parentDrop = ele.parents('.dropdown'),
			$allSidebars = $sidebar.find('.navlevel--0.dropdown');

		e.preventDefault();
		// disabled on mobile and active page menus
		if (!$parentDrop.hasClass('active')) {
			$parentDrop.toggleClass('dropdown-open');
			$(ele).blur();
			$sidebar.focus();
		}
	}


	// for browsers that don't support focus-within for tabbing
	function focusSideNav(ele) {
		if (ele.parents('.dropdown--main').length) {
			LOY.util.primaryFocusNav(ele, '.dropdown--main');  // primary nav dropdown

			if (ele.parents('.navlevel--1').length) {  // secondary nav dropdown
				LOY.util.secondaryFocusNav(ele, '.navlevel--1');
			}

		} else if (ele.parents('.dropdown').length) {  // standard dropdown menus
			LOY.util.primaryFocusNav(ele, '.dropdown');

		} else {  // no dropdown
			$('.dropdown, .dropdown--main').removeClass('focus-nav');
		}
	}




	function bindEvents() {  // event handlers
		$(window).on('resize', LOY.util.debounce(function() {  // disable active mobile menu on desktop
			(($(window).width() > desktopSize) ? $sidebar.find('nav').removeClass('menu-active') : null);
		}, 250));

		// open and close mobile page sidebar menu
		$sidebar.on('click', 'nav h2', function(e) {
			mobileSidebarMenu($(this), e);
		});

		// $sidebar.on('click', 'nav h2', function(e) {
		// 	mobileSidebarMenu($(this), e);
		// });

		// open and close default drupal sidebar menus
		$sidebar.on('click', '.navlevel--0.dropdown > .parent--anchor', function(e) {
			if ($(window).width() >= desktopSize) {
				desktopSidebarMenu($(this), e);
			}
		});

		// change focus for tab accessibility
		$sidebar.on('keydown', function(e) {
			var activeElement = document.activeElement,
				$parentDrop = $(this).parents('li');
			$parentDrop.removeClass('dropdown-open');
		});
	}




	return {
		init: init,
		bindEvents: bindEvents,
		focusSideNav: focusSideNav
	};

})(LOY, jQuery);