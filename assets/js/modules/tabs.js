LOY.tabs = (function(LOY, $) {
	var desktopSize = LOY.desktopSize || 992;



	function init() {
		document.onreadystatechange = function() {
			if (document.readyState === 'complete') {
				checkTabTypes();
			}
		}

		bindEvents();
	}



	// set tab conditions based on component class
	function checkTabTypes() {
		if (document.querySelector('.tabs--news') !== null) {
			// setTabs('.tabs--news');
		}
		if (document.querySelector('.tabs--icon') !== null) {
			setTabs('.tabs--icon');
		}
		LOY.images.lazyImage('.lazyload');
	}


	// get all height values for tabs
	function getTabHeights(tabs) {
		var heights = [];

		for (var i = 0; i < tabs.length; i++) {
			heights.push(tabs[i].offsetHeight);
		}

		return heights;
	}


	// set equal tab heights
	function matchTabHeights(ele, tabs, reset) {
		var $allTabs = document.querySelectorAll(tabs),
			tabHeights = getTabHeights($allTabs),
			tallestTab = '';

		if (reset !== undefined && reset !== null) {
			tallestTab = 'auto';
		} else {
			tallestTab = LOY.util.findLargest(tabHeights) + 'px';
		}

		modifyHeight($allTabs, tallestTab);
	}


	// modify tab heights
	function modifyHeight(tabName, height) {
		for (var i = 0; i < tabName.length; i++) {
			tabName[i].style.height = height;
		}
	}


	// determine tab settings
	function setTabs(tabName) {
		if (document.querySelector(tabName) !== null && window.innerWidth > desktopSize) {
			matchTabHeights(tabName, '.tab-pane', null);
		} else if (document.querySelector(tabName) !== null) {
			matchTabHeights(tabName, '.tab-pane', 'reset');
		}
	}



	function bindEvents() {  // event handlers
		$(window).on('resize', LOY.util.debounce(function() {
			if (document.querySelector('.tabs') !== null) {
				checkTabTypes();
			}
		}, 400));
	}




	return {
		init: init,
		matchTabHeights: matchTabHeights
	};

})(LOY, jQuery);