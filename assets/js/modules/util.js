LOY.util = (function(LOY, $) {  // utility functions
	var $body = document.querySelector('body');
		// focusHandler = ally.style.focusWithin();


	function init() {
		// check document state
		document.addEventListener("DOMContentLoaded", function() {
			browserCheck();
			externalLinkStyle();
			pageLoadEvents();
		});

		bindEvents();
	}


	// Determine browser features
	function browserCheck() {
		// detect IE11 browser
		if (!(window.ActiveXObject) && "ActiveXObject" in window) {
			$body.classList.add('ie');
		}
		// detect smooth scroll CSS property capability
		if ('scrollBehavior' in document.documentElement.style) {
			$body.classList.add('smoothscroll');
		}
	}


	// Check url domain source
	function checkDomain(url) {
		if ( url.indexOf('//') === 0 ) {
			url = location.protocol + url;
		}
		return url.toLowerCase().replace(/([a-z])?:\/\//,'$1').split('/')[0];
	}


	// Test for external link
	function isExternalLink(url) {
		return (( url.indexOf(':') > -1 || url.indexOf('//') > -1 ) && checkDomain(location.href) !== checkDomain(url));
	}


	// Test for mail or tel links
	function isContactLink(url) {
		return (url.indexOf('mailto:') !== -1 || url.indexOf('tel:') !== -1 || url.indexOf('sms:') !== -1);
	}


	// Display visual indicator for external links
	function externalLinkStyle() {
		var linkItem = document.getElementsByTagName('a');

		for (var i = 0; i < linkItem.length; i++) {

			if (isExternalLink(linkItem[i].href) && !isContactLink(linkItem[i].href)) {
				var textTitle = linkItem[i].getAttribute('title') || linkItem[i].textContent,
					newWindow = linkItem[i].getAttribute('target'),
					newTitle = textTitle + ' (Link will open in a new window/tab)';

				linkItem[i].classList.add('ext');

				if (newWindow !== null) {
					// console.info(newWindow, linkItem[i].textContent);
					linkItem[i].setAttribute('rel', 'noopener');
					linkItem[i].setAttribute('title', newTitle);
				}
			}
		}
	}


	// Display elements on page load if JS is enabled
	function pageLoadEvents() {
		setTimeout(function() {
			document.querySelector('body').classList.remove('js-loading');
		}, 400);
	}


	// standard debounce function from davidwalsh.name
	// Returns a function, that, as long as it continues to be invoked, will not
	// be triggered. The function will be called after it stops being called for
	// N milliseconds. If `immediate` is passed, trigger the function on the
	// leading edge, instead of the trailing.
	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	}


	// cookie utils from http://www.quirksmode.org/js/cookies.html
	function createCookie(name,value,days) {
		var date, expires;
		if (days) {
			date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = ";expires="+date.toGMTString();
		} else {
			expires = "";
		}
		document.cookie = name+"="+value+expires+"; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}

	function eraseCookie(name) {
		createCookie(name, '', -1);
	}


	// Check for localstorage support
	function storageAvailable(type) {
		try {
			var storage = window[type],
				x = '__storage_test__';
			storage.setItem(x, x);
			storage.removeItem(x);
			return true;
		}
		catch(e) {
			return e instanceof DOMException && (
				// everything except Firefox
				e.code === 22 ||
				// Firefox
				e.code === 1014 ||
				// test name field too, because code might not be present
				// everything except Firefox
				e.name === 'QuotaExceededError' ||
				// Firefox
				e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
				// acknowledge QuotaExceededError only if there's something already stored
				storage.length !== 0;
		}
	}


	// Find largest number in arrary
	function findLargest(arr) {
		return arr.reduce(function(prevLargest, currLargest) {
			return (currLargest > prevLargest) ? currLargest : prevLargest;
		}, 0);
	}


	// Smooth scroll links on page (if scroll behavior is not supported)
	function smoothScroll(ele, evt) {
		var tgt = $(ele).attr('href'),
			scrollSpeed = parseInt($(ele).attr("data-scrollspeed")) || 500;

		evt.preventDefault();

		$('html, body').animate({
			// scrolls to link position
			scrollTop: $(tgt).offset().top
		}, scrollSpeed);

		return false;
	}


	// prevent page scroll for active modules
	function lockScroll(forceState) {
		var $htmlClasses = document.querySelector('html').classList;

		if ($htmlClasses.contains('scroll-lock') || forceState === 'off' ) {
			$htmlClasses.remove('scroll-lock');
		} else {
			$htmlClasses.add('scroll-lock');
		}
	}


	// reset targeted menu class
	function resetMenu(targetClass) {
		var $body = document.querySelector('body');

		lockScroll('off');
		($body.classList.contains(targetClass) ? $body.classList.remove(targetClass) : null);
	}


	// trap user tab focus within modules. expects object with parent container and selectable element
	function tabTrap(itemObj, e) {
		var $focusedElementBeforeModal,
			$focusableElements = $(itemObj.parent).find(itemObj.element),
			firstTabStop = $focusableElements[0],
			lastTabStop = $focusableElements[$focusableElements.length - 1],
			keyNum = 'which' in e ? e.which : e.keyCode;

		$focusedElementBeforeModal = document.activeElement;

		// Check for TAB key press
		if (keyNum === 9) {
			if (e.shiftKey) { // SHIFT + TAB
				if (document.activeElement === firstTabStop) {
					e.preventDefault();
					lastTabStop.focus();
				}
			} else { // TAB
				if (document.activeElement === lastTabStop) {
					e.preventDefault();
					firstTabStop.focus();
				}
			}
		} else if (keyNum === 27) {  // ESC key
			$focusedElementBeforeModal.focus();
		}
	}


	// removes loader and fires callback after timeout.
	// expects an object for options with { ele: elementname, timer: timeout timer, className: class to remove }
	function loadingComplete(options, callback) {
		var item = $(options.ele),
			timer = options.timer || 250,
			className = options.className || 'loading';

		setTimeout(function() {
			// console.info('removing loader');
        	item.removeClass(className);

			if (typeof callback === 'function') {
				// console.info('callback running');
				callback();
			}
        }, timer);
	}


	// remove class from all matching elements
	function resetAllClasses(ele, className) {
		var $elements = document.querySelectorAll(ele);

		for (var i = 0; i < $elements.length; i++) {
			$elements[i].classList.remove(className);
		}
	}


	// wrap a (jQuery) element with a div
	function wrapItems(ele, wrapperName) {
		$(ele).each(function() {
			$(this).wrap('<div class="' + wrapperName + '"></div>');
		});
	}


	// generate a random number integer between two values
	function randomNumberGen(min, max) {
		var min = Math.ceil(min),
			max = Math.floor(max);

		return Math.floor(Math.random() * (max - min)) + min;
	}


	// determine tab keypress intention
	function routeTabs() {
		var $active = $(':focus');

		if ($active.parents('.header__main').length) {  // nav dropdown
			LOY.header.focusNav($active);
		} else if ($active.parents('.form-checkboxes').length && $('.finder-view__filters').length) {  // program finder
			// console.info('program finder');
		} else if ($active.parents('.dropdown--main').length && $active.parents('.sidebar').length) {  // sidebar
			LOY.sidebar.focusSideNav($active);
		}
	}


	// add a class to parent nav elements
	function primaryFocusNav(ele, parentClass) {
		var $parentDrop = ele.parents(parentClass);

		console.info(parentClass);

		$('.focus-nav').removeClass('focus-nav');  // remove all active states

		if (!$parentDrop.hasClass('focus-nav')) {  // add active state to inactive parent
			console.info('add focus class');
			$parentDrop.addClass('focus-nav');
		}
	}


	// add an active class to secondary nav elements
	function secondaryFocusNav(ele, parentClass) {
		if (ele.parents(parentClass)) {
			console.info('add focus secondary');
			ele.parents(parentClass).addClass('focus-nav');
		} else {
			$(parentClass).removeClass('focus-nav');
		}
	}



	function bindEvents() {  // event handlers
		// smooth scroll links with JS for browsers that do not support CSS scroll-behavior
		$('.scroll-link').on('click', function(e) {
			if (!$('.smoothscroll').length) {
				// console.info('smooth scroll css property not supported')
				smoothScroll($(this), e);
			}
		});

		// keypress actions
		document.addEventListener('keyup', function(e) {
			var $htmlClasses = document.querySelector('html').classList,
				$bodyClasses = document.querySelector('body').classList,
				keyNum = 'which' in e ? e.which : e.keyCode;

			if (keyNum === 27 && $htmlClasses.contains('scroll-lock')) {  // esc key
				($bodyClasses.contains('quicklinks--open') ? resetMenu('quicklinks--open') : null);
				($bodyClasses.contains('mobilenav--open') ? resetMenu('mobilenav--open') : null);
			} else if (keyNum === 9 && $htmlClasses.contains('no-focuswithin')) {  // browsers that don't support focus-within for tabbing
				routeTabs();
			}
		});
	}



	return {
		init: init,
		createCookie: createCookie,
		debounce: debounce,
		eraseCookie: eraseCookie,
		findLargest: findLargest,
		loadingComplete: loadingComplete,
		lockScroll: lockScroll,
		randomNumberGen: randomNumberGen,
		readCookie: readCookie,
		resetAllClasses: resetAllClasses,
		smoothScroll: smoothScroll,
		storageAvailable: storageAvailable,
		primaryFocusNav: primaryFocusNav,
		secondaryFocusNav: secondaryFocusNav,
		wrapItems: wrapItems
	};

})(LOY, jQuery);