LOY.video = (function(LOY, $) {


	function init() {
		bindEvents();
	}



	function bindEvents() {  // event handlers

		// inline video player
		$('body').on('click', '.video__inline', function(e) {
			var $el = $(this),
				$vid = $el.find('video'),
				$icon = $el.find('.fa'),
				videoElement = $vid[0];

			e.preventDefault();

			$icon.toggleClass('fa-play-circle fa-pause-circle');

			if ($el.hasClass('playing')) {  // pause video
				$el.removeClass('playing');
				videoElement.pause();
			} else {  // play video
				$el.addClass('playing');
				videoElement.play();
			}
		});
	}


	return {
		init: init
	};

})(LOY, jQuery);