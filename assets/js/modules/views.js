LOY.views = (function(LOY, $) {
	var $view = '.views-element-container';



	function init() {
		bindEvents();
	}



	function bindEvents() {  // event handlers
		// check for ajax load
        $(document).ajaxStart(function() {
            $($view).addClass('loading');
        });

		// check for ajax completion
        $(document).ajaxComplete(function() {
			var options = {
				ele: '.views-element-container',
			};
			LOY.util.loadingComplete(options);

			if (document.querySelector('.lazyload:not(.loaded)') !== null || document.querySelector('.lazyload--bkgd:not(.loaded)') !== null) {
				LOY.images.init();
			}

			if (document.querySelector('.masonry-view') !== null) {
				LOY.masonry.init();
			}
        });
	}


	return {
		init: init
	};

})(LOY, jQuery);