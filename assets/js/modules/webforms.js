LOY.webforms = (function(LOY, $) {
	var $webform = $('.webform-submission-form');



	function init() {
		enableValidation();
	}


	// to prevent false positives on validation, only enable validation once the user has focused on the form
	function enableValidation() {
		$webform.one('focusin', function() {
			$webform.addClass('validate');
		});
	}



	return {
		init: init
	};

})(LOY, jQuery);