# Loyola Components

Components in the Loyola theme require the [Paragraphs module](https://www.drupal.org/project/paragraphs) to display the custom content structures we have defined.

## Templates

Templates for components can be found in the `./templates/  ` directory. The full list of all user-accessible components are listed below.

|     Name     |        Template(s)        |   CSS module   |  JS  |
|--------------|------------------------|----------------|-------------|
| Carousel | `paragraph--carousel-slider.html.twig` | `_slider.scss` | `carousel.js` |
| Content Tout - Two Column | `paragraph--content-touts-display-text-throu.html.twig` | `touts/_content-tout-two-col.scss` | -- |
| Content Tout with Image | `paragraph--content-card-with-focal-image.html.twig` | `content-card/_content-card-image.scss` | -- |
| Accordion |`paragraph--bp-accordion.html.twig`|`_accordion.scss` | -- |
|     Hero     |`paragraph--hero_banner.html.twig`| `_hero.scss` | `hero.js`|
| Icon Tout |`paragraph--icon-tout.html.twig`|`touts/_icon-tout.scss`  | -- |
| Image Accordion |  `paragraph--image-tout-gallery.html.twig`  | `touts/_image-tout-gallery.scss`  |  -- |
| Image Tout - Full Width |`paragraph--one-up-tout.html.twig`| `touts/_image-tout-full-width.scss` | -- |
| Image Tout - Full Width - Two Column Text |`paragraph--two-up-tout.html.twig`| `touts/_image-tout-two-col-text.scss` | -- |
| Image Tout - Two Column |`paragraph--two-up-image-tout.html.twig`| `touts/_image-tout-two-col-image.scss` | -- |
| News Cards | `paragraph--news_card.html.twig` | `content-card/_content-card-news.scss` | -- |
| News Tabs | `paragraph--news_tab_section.html.twig` | `tabs/_tabs-news.scss` | `tabs.js` |
| Quick Links | `paragraph--quick-links.html.twig` | `./assets/sass/global/quicklinks.scss` | `quicklinks.js` |
| Steps | `paragraph--steps-component.html.twig` | `_steps.scss` | -- |
| Tabbed Component | `paragraph--tabbed-icon-tout.html.twig`, `paragraph--tabbed-icon-tout-section.html.twig` | `tabs/_tabs-icons.scss` | `tabs.js` |


## Styling

Component styling is broken out within the `./assets/sass/components` directory. In most cases, components will have their own stylesheet, which will be combined during the build process (see `README.md` under the section "CSS/Sass" for more detail). Components with similar styling or structure are combined into their own subfolders (for example, `/touts/` for the various tout types).


## JavaScript
Individual component JavaScript files are located in `./assets/js/modules/` and are conditionally loaded in through the `./assets/js/base/router.js` file.
