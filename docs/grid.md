# Loyola Grid Layout

There are two different methods available for building layouts. This theme primarily uses [flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) to construct pages. A simplified version of this grid is available for use in creating page layouts, in addition to the underlying Bootstrap grid structure.

------

## Flexbox grid

The theme's grid layout can be created by adding class `component__grid` onto a containing HTML element (`<div>`, `<section>`,  `<ul>`, `<article>`, etc) and designating each child item with class `grid__item`.

In the example below, `.component__grid` is the parent containing `<div>` of two `.grid__item` children:
```
<div class="component">
	<div class="component__grid">
		<div class="grid__item grid--3">This is an example item</div>
		<div class="grid__item grid--3">Another example here</div>
        <div class="grid__item grid--3">Here's a third example item</div>
	</div>
</div>
```

*Example 1—A basic three column grid.*

*NOTE: the `<div class="component">` container is not required for the grid.*

Because this grid is designed to be [mobile-first](https://zurb.com/word/mobile-first), all child items will display at 100% of the parent container's width until the theme's desktop breakpoint at `$screen-md` (defined in `./assets/sass/base/_variables.scss`) has been reached.

At the `$screen-md` breakpoint, each child item's width will then remain at 100% width, unless specified with one of the width class definitions listed in the table below:

| # of items | Classname | Width |
|------------|---------- | ----- |
|      2     |`.grid--2` | 50%   |
|      3     |`.grid--3` | 33.3% |
|      4     |`.grid--4` | 25%   |
|      5     |`.grid--5` | 20%   |
|      6     |`.grid--6` | 16.6% |

In Example 1 above, a three column grid has been constructed using the `.grid--3` class on each child item.

*NOTE: typically when building a grid of items, spacing (padding) between items should be assigned to the child of the grid item and not the grid item itself. This will avoid issues with grid items breaking outside of the row.*

------


## Bootstrap grid

```
<div class="container">
    <div class="row">
        <div class="col-md-4">This is an example item</div>
		<div class="col-md-4">Another example here</div>
        <div class="col-md-4">Here's a third example item</div>
	</div>
</div>
```

*Example 2—A Bootstrap layout with sidebar*.

*NOTE: At time of development, this theme was based upon [Bootstrap 3.3.7](https://getbootstrap.com/docs/3.3/), as Bootstrap 4 had not yet been released.*

As specified in the primary documentation, Bootstrap is in use throughout this theme, so a [normal Bootstrap layout](https://getbootstrap.com/docs/3.3/css/#grid) can also be applied throughout the site, with a few exceptions.

Additional documentation on building a Bootstrap grid can be found [on the Bootstrap site](https://getbootstrap.com/docs/3.3/css/#grid).

